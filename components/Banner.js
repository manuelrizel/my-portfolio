import { Button, Row, Col } from 'react-bootstrap';
import { IoCodeSlashOutline } from 'react-icons/io5'



export default function Banner(){
	return (
	<div className="banner-section" id="home">
		<div className="col-md-12">
		<IoCodeSlashOutline size={80} />
  		<h1>Hello, I'm Rizel.</h1>
		  	<p>
		    	Full Stack Web Developer
		  	</p>
		 </div>
	</div>
	)
}