import { Button, Row, Col } from 'react-bootstrap';
import { BiMailSend } from "react-icons/bi";
import { AiFillLinkedin, AiFillGitlab } from "react-icons/ai";





export default function Contacts(){
	return (
	<div className="contact-section">
		<div className="col-md-12">
		<div className="row">	
				<div className="col email-section">
				<p>	Rizel Manuel &#169; 2021  </p>
				
				</div>
			
				<div className="col social-box">
				<a href="mailto:manuelrizel@gmail.com"><BiMailSend size={30} className="social-icons"/></a>
					<a href="https://www.linkedin.com/in/rizel-manuel-179962204/" target="_blank"><AiFillLinkedin size={30} className="social-icons" /></a>
					<a href="https://gitlab.com/manuelrizel" target="_blank"><AiFillGitlab size={30} className="social-icons" /></a>
				</div>
		 	</div>
		 </div>
	</div>
	)
}