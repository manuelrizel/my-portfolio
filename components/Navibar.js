import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import { CgCode, CgCodeSlash } from "react-icons/cg";


export default function Navibar(){

	return (
		<Navbar variant="dark" fixed="top" className="navbar">
		  	<Nav className="m-auto">
		  	<div className="navIcons">
		  		<CgCode />
		  	</div>
			    <Link href="#home">

	          			<a className="nav-link"> home </a>
	        	</Link>
			    <Link href="#skills">
	          			<a className="nav-link"> skills </a>
	        	</Link>
	        	<Link href="#projects">
	          			<a className="nav-link"> projects </a>
	        	</Link>
	        	<div className="navIcons">
	        	<CgCodeSlash />
	        	</div>
			</Nav>
		</Navbar>
		)
}