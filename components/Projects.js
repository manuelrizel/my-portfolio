import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import projectData from '../data/projects';
import Link from 'next/link';
import { FiExternalLink } from "react-icons/fi";


export default function Project(){
console.log(projectData)

const projects = projectData.map(project => {
		console.log(project)
		return(
					<div className="col-md-4 project-tile">
						<Card key={project.projId} className="card-container">
						  <Card.Img variant="top" src={project.srcPic}/>
						  <Card.Body className="card-body">
						    <Card.Title>{project.title} 
						    <Link href={project.link}>
						   	 	<a target="_blank"><FiExternalLink className="link-icon" />	</a>
						    </Link>
						    </Card.Title>
						    <Card.Text className="card-text">
						   {project.description}
						    </Card.Text>
						  </Card.Body>
						</Card>
					</div>
			)
	})

return (

			<div className="portfolio-section ">
			 <div className="col-md-12">
				<div id="projects">
			    	<h1>Projects</h1>
			    </div>

			    <div className="row">
					    {projects}
				</div>
				</div>
			</div>
		)
}
