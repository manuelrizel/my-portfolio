import { Container } from 'react-bootstrap';
import { IoLogoHtml5, IoLogoCss3, IoLogoNodejs, IoLogoReact  } from 'react-icons/io5'
import { DiMongodb, DiJavascript, DiPhotoshop  } from 'react-icons/di';
import { SiAdobephotoshop, SiBootstrap, SiSublimetext  } from 'react-icons/si';
import { FaGitlab, FaElementor, FaWordpressSimple } from "react-icons/fa";



export default function Skills(){
	return (
	<div className="skills-section" id="skills">
		<div>
			<h1>What do I know?</h1>
			<p> These are some of the tools/technologies that I have an experience with.</p>	
				<div className="skills-box col-md-12">
					<div className="row">
						<div className="col icon-box">
							<IoLogoHtml5 size={50} className="skill-icon" />
			          		<p>HTML 5</p>
			          	</div>
			          	<div className="col icon-box">
							<IoLogoCss3 size={50} className="skill-icon" />
			          		<p>CSS 3</p>
			          	</div>
						<div className="col icon-box">
							<DiJavascript size={50} className="skill-icon" />
			          		<p>Javascript</p>
			          	</div>
			          	<div className="col icon-box">
							<FaGitlab size={50} className="skill-icon" />
			          		<p>GitLab</p>
			          	</div>
			          	<div className="col icon-box">
							<SiBootstrap size={50} className="skill-icon" />
			          		<p>Bootstrap</p>
			          	</div>
			          	<div className="col icon-box">
							<IoLogoNodejs size={50} className="skill-icon" />
			          		<p>Node JS</p>
			          	</div>
					</div>
					<div className="row">
			          	<div className="col icon-box">
							<DiMongodb size={50} className="skill-icon" />
			          		<p>Mongo DB</p>
			          	</div>
			          	<div className="col icon-box">
							<IoLogoReact size={50} className="skill-icon" />
			          		<p>React JS</p>
			          	</div>
			          	<div className="col icon-box">
							<SiSublimetext size={50} className="skill-icon" />
			          		<p>Sublime</p>
			          	</div>
			          	<div className="col icon-box">
							<FaWordpressSimple size={50} className="skill-icon" />
			          		<p>Wordpress</p>
			          	</div>
			          	<div className="col icon-box">
							<FaElementor size={50} className="skill-icon" />
			          		<p>Elementor</p>
			          	</div>
			          	<div className="col icon-box">
							<DiPhotoshop size={50} className="skill-icon" />
			          		<p>Photoshop</p>
			          	</div>
			    	</div>
	
		          </div>
		</div>
	</div>

		)
}