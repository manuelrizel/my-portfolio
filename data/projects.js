const projectData = [
	{
		projId: "01",
		title: "Genesis Academy",
		description: "A course booking system that was one of the capstone projects I did during my Zuitt Bootcamp. I used HTML, CSS, Bootstrap, Javascript, ExpressJS, NodeJS, and MongoDB.  ",
		srcPic: "/images/genesis.JPG",
		link: "https://manuelrizel.gitlab.io/cs2_frontend/"
	},
	{
		projId: "02",
		title: "ThriftyWallet",
		description: "ThriftyWallet is my final capstone project. It is a budget tracking application which was made using MERNN Stack (MongoDB, Express.js, React.js, Node.js, Next.js)",
		srcPic: "../images/thriftywallet.JPG",
		link: "https://budget-tracker-cs3-seven.vercel.app/"
		
	},
	{
		projId: "03",
		title: "Mortgages Physician",
		description: "Mortgages Physician is the first project I've built with my part time job, mostly about blogs and I was responsible for the template using Wordpress and Elementor.",
		srcPic: "../images/mortgagephysician.JPG",
		link: "https://www.mortgagesphysician.com/"
		
	}
]

export default projectData;
