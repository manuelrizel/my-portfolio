import '../styles/globals.css'
import { Container } from 'react-bootstrap';
//react bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';
//relative imports
import Navibar from '../components/Navibar'



function MyApp({ Component, pageProps }) {


  return (
  	<div >
  	 <Navibar />
	 	<Component {...pageProps} />
  	</div>
  	)
}

export default MyApp
