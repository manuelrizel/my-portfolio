import Head from 'next/head'

import Banner from '../components/Banner'
import Skills from '../components/Skills'
import Projects from '../components/Projects'
import Contacts from '../components/Contacts'
export default function Home() {
  return (
  	<>
      <Head>
        <title>Rizel Manuel | Portfolio </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Banner />
      <Skills />
      <Projects />
      <Contacts />
     </>
  )
}
